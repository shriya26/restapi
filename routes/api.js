const express = require('express');
const router=express.Router();
const Person = require('../models/persons');

//get a list of teachers from the db 
router.get('/persons',function (req,res,next) {
	Person.aggregate().near({
   near: {
    'type': 'Point',
    'coordinates': [parseFloat(req.query.lng), parseFloat(req.query.lat)]
   },
   maxDistance: 100000,
   spherical: true,
   distanceField: "dis"
  })﻿.then(function(person){
  	res.send(person);
  });
});


//add a new teacher to the db
router.post('/persons',function (req,res,next) {
	Person.create(req.body).then(function(person){
		res.send(person);
	}).catch(next);
});//create : to send data to mongodb and save it
//next : is for error handler(middleware called) so if there is an error then move to error handling middleware

//update a person details
router.put('/persons/:id',function (req,res,next) {
	Person.findByIdAndUpdate({_id:req.params.id},req.body).then(function () {
		Person.findOne({_id:req.params.id}).then(function (person) {
			res.send(person);
		});
	});	
});

//delete a person from db
router.delete('/persons/:id',function (req,res,next) {
	// console.log(req.params.id);
	Person.findByIdAndRemove({_id:req.params.id}).then(function (person) {
		res.send(person);
	});
});// 

module.exports=router;
