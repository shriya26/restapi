const mongoose =require('mongoose');
const Schema = mongoose.Schema;

//create geolocation Schema
const GeoSchema = new Schema({
	type:{
		type:String,
		default:"Point"
	},
	coordinates:{
		type:[Number],
		index:"2dsphere"
	}
});



//create person schema and model
const PersonSchema= new Schema({
	name:{
		type:String,
		required:[true,'Name field is require']
	}, 
	subject:{
		type:String
	},
	available:{
		type:Boolean,
		default:false
	},

	//add in geo location
	geometry:GeoSchema
});

const Person= mongoose.model('Person',PersonSchema);

module.exports=Person;